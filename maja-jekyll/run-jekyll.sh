#!/bin/bash

docker run -w /site -it -v /home/majak/ubuntu/analysis-documentation:/site maja/jekyll:jekyll bundle install
docker run -it -p 4000:4000 -v /home/majak/ubuntu/analysis-documentation:/site maja/jekyll:jekyll
