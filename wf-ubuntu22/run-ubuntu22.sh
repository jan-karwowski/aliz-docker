#!/bin/bash

docker run --rm -it -v/home/majak/ubuntu:/home/maja:rw  \
  -v/data8/majak:/data8/majak:rw \
  -v/:/host \
  --shm-size=2g \
  -e  "TERM=xterm-256color" \
  --net=host \
  --cap-add=SYS_ADMIN \
  maja/ubuntu22:ubuntu22 \
  bash --init-file /home/maja/.bashrc


#docker exec -it $(docker ps --filter name=my-service -q) bash -c 'PERF=/usr/bin/perf record -a -F 999 -g /home/maja/tests/mixing-benchmark/run-bench.sh'
